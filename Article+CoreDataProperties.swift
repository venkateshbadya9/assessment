//
//  Article+CoreDataProperties.swift
//  
//
//  Created by Venkatesh on 7/9/20.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var comments: Int64
    @NSManaged public var content: String?
    @NSManaged public var createdAt: String?
    @NSManaged public var likes: Int64
    @NSManaged public var user: User?
    @NSManaged public var media: Media?

}
