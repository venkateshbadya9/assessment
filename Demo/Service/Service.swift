import UIKit

class Service: NSObject {
    static let sharedInstance = Service()
    var isLoadMore: Bool = true
    
    // MARK: - API Call
    
    /// Calling API to fetch Article Data
    func getArticleData(pageIndex:Int, completion: @escaping([ArticleModel]?, Error?) -> ()) {
        let urlString = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/blogs?page=\(pageIndex)&limit=10"
        guard let url = URL(string: urlString) else { return }
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 5.0
        sessionConfig.timeoutIntervalForResource = 5.0
        let session = URLSession(configuration: sessionConfig)
        session.dataTask(with: url) { (data, response, error) in
            if let err = error {
                self.isLoadMore = true
                completion(nil, nil)
                print("Error while fetching : \(err.localizedDescription)")
            } else {
                guard let data = data else { return }
                do {
                    let results = try JSONDecoder().decode([ArticleModel].self, from: data)
                    self.isLoadMore = (results.count > 0) ? true : false
                    completion(results, nil)
                } catch let jsonError {
                    print("Json Error : \(jsonError.localizedDescription)")
                    completion(nil, nil)
                }
            }
        }.resume()
    }
}
