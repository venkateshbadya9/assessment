import UIKit

/// UserModel
struct UserModel: Codable {
     var name: String?
     var designation: String?
     var avatar: String?
    
    init(name: String, designation: String, avatar: String) {
        self.name = name
        self.designation = designation
        self.avatar = avatar
    }
}

/// MediaModel
struct MediaModel: Codable {
     var image: String?
     var title: String?
     var url: String?
    
    init(image: String, title: String, url: String) {
        self.image = image
        self.title = title
        self.url = url
    }
}

/// ArticleModel
struct ArticleModel: Codable {
     var createdAt: String?
     var content: String?
     var comments: Int?
     var likes: Int?
    
     var user: [UserModel]?
     var media: [MediaModel]?
    
    init(
        createdAt: String,
        content: String,
        comments: Int,
        likes: Int,
        user: [UserModel],
        media: [MediaModel]
    ) {
        self.createdAt = createdAt
        self.content = content
        self.comments = comments
        self.likes = likes
        self.user = user
        self.media = media
    }
}
