import UIKit

/// ArticleCellModel
struct ArticleCellModel {
    var userImageString: String
    var userName: String
    var userDesgn: String
    var publishTime: Date
    var articleImageString: String
    var articleContent: String
    var articleTitle: String
    var articleUrl: String
    var likes: Int
    var comments: Int
    var isMediaAvailable: Bool = false
}

