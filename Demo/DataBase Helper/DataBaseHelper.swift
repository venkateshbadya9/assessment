import Foundation
import UIKit
import CoreData

class DataBaseHelper: NSObject {
    static let sharedInstance = DataBaseHelper()
    var fetchOffSet = 0
    
    // MARK: - DataBase Methods
    
    /// Saving of ArticleData fetcched from server
    func saveData(articleInfo: [ArticleModel]) {
        DispatchQueue.main.async {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            guard let articleEntity = NSEntityDescription.entity(forEntityName: "Article", in: context),
                let userEntity = NSEntityDescription.entity(forEntityName: "User", in: context),
                let mediaEntity = NSEntityDescription.entity(forEntityName: "Media", in: context) else { return }
            
            articleInfo.forEach { article in
                let newUserEntity = NSManagedObject(entity: userEntity, insertInto: context)
                newUserEntity.setValue(article.user?.first?.avatar, forKey: "avatar")
                newUserEntity.setValue(article.user?.first?.designation, forKey: "designation")
                newUserEntity.setValue(article.user?.first?.name, forKey: "name")
                
                let newMediaEntity = NSManagedObject(entity: mediaEntity, insertInto: context)
                newMediaEntity.setValue(article.media?.first?.image, forKey: "image")
                newMediaEntity.setValue(article.media?.first?.title, forKey: "title")
                newMediaEntity.setValue(article.media?.first?.url, forKey: "url")
                
                let newArticleEntity = NSManagedObject(entity: articleEntity, insertInto: context)
                newArticleEntity.setValue(article.createdAt, forKey: "createdAt")
                newArticleEntity.setValue(article.content, forKey: "content")
                newArticleEntity.setValue(article.comments, forKey: "comments")
                newArticleEntity.setValue(article.likes, forKey: "likes")
                newArticleEntity.setValue(newUserEntity, forKey: "user")
                newArticleEntity.setValue(newMediaEntity, forKey: "media")
                
                do {
                    try context.save()
                    print("Saved")
                } catch {
                    print("Failed Saving")
                }
            }
        }
    }
    
    /// Fetching of ArticleData saved in DataBase
    /// Fetching in batches of 10
    func getArticleData() -> [Article]? {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
        request.fetchLimit = 10
        request.fetchOffset = fetchOffSet
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request) as? [Article]
            fetchOffSet = fetchOffSet + 10
            print("Fetched")
            return result
        } catch {
            print("Failed Fetching")
        }
        return []
    }
    
    /// Deleting of previously stored data from DataBase
    func deleteData() {
        DispatchQueue.main.async {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Article")
            let request = NSBatchDeleteRequest(fetchRequest: fetch)
            
            do {
                try context.execute(request)
                print("Deleted")
            } catch {
                print("Failed Deleting")
            }
        }
    }
}
