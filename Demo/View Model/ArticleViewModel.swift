import UIKit
import Reachability

class ArticleViewModel: NSObject {
    // MARK: Data
    
    private var articles = [ArticleModel]()
    let apiService: Service?
    var reloadTableView: (()->())?
    var updateFetchingStatus: (()->())?
    var dataBaseHelper = DataBaseHelper.sharedInstance
    
    private var cellModels = [ArticleCellModel]() {
        didSet {
            self.reloadTableView?()
        }
    }

    var isFetching: Bool = false {
        didSet {
            self.updateFetchingStatus?()
        }
    }
    
    var numberOfCells: Int {
        return cellModels.count
    }

    init( apiService: Service = Service()) {
        self.apiService = apiService
    }
    
    // MARK: - Fetching Methodology
    
    /// Online
    func fetchDataOnline(pageIndex: Int) {
        self.isFetching = true
        self.apiService?.getArticleData(pageIndex: pageIndex) { (article, error) in
            if error == nil {
                self.isFetching = false
                if article != nil {
                    self.dataBaseHelper.deleteData()
                    self.articles.append(contentsOf: article ?? [])
                    self.dataBaseHelper.saveData(articleInfo: self.articles)
                    self.processFetchedArticles(articles: self.articles)
                }
            }
        }
    }
    
    /// Offline
    func fetchDataOffline() {
        self.isFetching = false
        let articles: [Article] = self.dataBaseHelper.getArticleData() ?? []
        articles.forEach { article in
            let articleModel = ArticleModel(
                createdAt: article.createdAt ?? "",
                content: article.content ?? "",
                comments: Int(article.comments),
                likes: Int(article.likes),
                user: [UserModel(
                    name: article.user?.name ?? "",
                    designation: article.user?.designation ?? "",
                    avatar: article.user?.avatar ?? ""
                )],
                media: article.media != nil ? [MediaModel(image: article.media?.image ?? "",
                                        title: article.media?.title ?? "",
                                        url: article.media?.url ?? ""
                )] : []
            )
            self.articles.append(articleModel)
        }
        self.processFetchedArticles(articles: self.articles)
    }
    
    // MARK: - Processing Data
    
    /// Processing Articles
    func processFetchedArticles(articles: [ArticleModel]){
        var articleCellModels = [ArticleCellModel]()
        for article in self.articles {
            articleCellModels.append( createCellModel(article: article))
        }
        self.cellModels = articleCellModels
    }
    
    /// Creating CellModel
    func createCellModel( article: ArticleModel) -> ArticleCellModel {
        let articleInfo = article
        let userInfo = articleInfo.user?.first
        let mediaInfo = articleInfo.media?.first
        
        return ArticleCellModel(userImageString: userInfo?.avatar ?? "",
                                userName: userInfo?.name ?? "",
                                userDesgn: userInfo?.designation ?? "",
                                publishTime: formatDate(dateString: articleInfo.createdAt ?? "") ?? Date(),
                                articleImageString: mediaInfo?.image ?? "",
                                articleContent: articleInfo.content ?? "",
                                articleTitle: mediaInfo?.title ?? "",
                                articleUrl: mediaInfo?.url ?? "",
                                likes: articleInfo.likes ?? 0,
                                comments: articleInfo.comments ?? 0,
                                isMediaAvailable: (mediaInfo != nil && mediaInfo?.image != "") ? false : true)
    }
    
    /// Fetching CellModel
    func getCellModel(at indexPath: IndexPath ) -> ArticleCellModel {
        return cellModels[indexPath.row]
    }
    
    /// Date Formatting
    func formatDate(dateString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date (from: dateString)
        return date
    }
}
