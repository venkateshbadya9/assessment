import UIKit

class ArticleCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userDesgnLbl: UILabel!
    @IBOutlet weak var publishTimeLbl: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleContentLbl: UILabel!
    @IBOutlet weak var articleTitleLbl: UILabel!
    @IBOutlet weak var articleUrlLbl: UILabel!
    @IBOutlet weak var likesLbl: UILabel!
    @IBOutlet weak var commentsLbl: UILabel!
    @IBOutlet weak var viewForImage: UIView!
    
    /// Initial Configuration
    
    var articleCellModel: ArticleCellModel? {
        didSet {
            userImageView.sd_setImage(with: URL(string: articleCellModel?.userImageString ?? ""), placeholderImage: nil)
            userNameLbl.text = articleCellModel?.userName
            userDesgnLbl.text = articleCellModel?.userDesgn
            publishTimeLbl.text = articleCellModel?.publishTime.timeAgoDisplay()
            articleImageView.sd_setImage(with: URL(string: articleCellModel?.articleImageString ?? ""), placeholderImage: nil)
            articleTitleLbl.text = articleCellModel?.articleTitle
            articleUrlLbl.text = articleCellModel?.articleUrl
            articleContentLbl.text = articleCellModel?.articleContent
            likesLbl.text = (articleCellModel?.likes.formatUsingAbbrevation() ?? "") + " Likes"
            commentsLbl.text = (articleCellModel?.comments.formatUsingAbbrevation() ?? "") + " Comments"
            if let isMedia = articleCellModel?.isMediaAvailable {
                viewForImage.isHidden = isMedia
                articleTitleLbl.isHidden = isMedia
                articleUrlLbl.isHidden = isMedia
            }
        }
    }
}
