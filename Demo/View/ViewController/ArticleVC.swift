import UIKit
import SDWebImage
import Reachability

class ArticleVC: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var articleTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Data
    
    lazy var articleViewModel: ArticleViewModel = {
        return ArticleViewModel()
    }()
    var pageIndex: Int = 1
    private var reachability : Reachability!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /// Recchability Observer
        self.reachability = try? Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
          try reachability.startNotifier()
        }catch{
          print("could not start reachability notifier")
        }
        startUp()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    // MARK: Rechability
    
    /// Checking for Rechability
    @objc func reachabilityChanged(note: Notification) {
      let reachability = note.object as! Reachability
      switch reachability.connection {
      case .cellular:
          print("Network available via Cellular Data.")
          break
      case .wifi:
          print("Network available via WiFi.")
          break
      case .none:
          print("Network is not available.")
          break
      case .unavailable:
          print("Network is  unavailable.")
          break
      }
    }
    
    
    // MARK: - Configuration
    
    /// Startup Configuration
    func startUp() {
        articleViewModel.updateFetchingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.articleViewModel.isFetching ?? false
                if isLoading {
                    UIView.animate(withDuration: 0.2, animations: {
                    })
                    self?.activityIndicator.isHidden = false
                }else {
                    UIView.animate(withDuration: 0.2, animations: {
                    })
                    self?.activityIndicator.isHidden = true
                }
            }
        }
        
        articleViewModel.reloadTableView = { [weak self] () in
            DispatchQueue.main.async {
                self?.articleTableView.reloadData()
            }
        }
            
        getArticleData()
    }
    
    /// Calling API to fetch data
    func getArticleData() {
        if reachability.connection != .unavailable {
            self.articleViewModel.fetchDataOnline(pageIndex: self.pageIndex)
            self.pageIndex = self.pageIndex + 1
        } else {
            self.articleViewModel.fetchDataOffline()
            articleViewModel.apiService?.isLoadMore = true
        }
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension ArticleVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleViewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articleCellIdentifier", for: indexPath) as! ArticleCell
        cell.layoutIfNeeded()
        let cellModel = articleViewModel.getCellModel(at: indexPath )
        cell.articleCellModel = cellModel
        return cell
    }
}

// MARK: - UIScrollViewDelegate

extension ArticleVC: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == articleTableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) && articleViewModel.apiService?.isLoadMore == true{
                articleViewModel.apiService?.isLoadMore = false
                getArticleData()
            }
        }
    }
}
