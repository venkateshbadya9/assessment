//
//  Media+CoreDataProperties.swift
//  
//
//  Created by Venkatesh on 7/9/20.
//
//

import Foundation
import CoreData


extension Media {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Media> {
        return NSFetchRequest<Media>(entityName: "Media")
    }

    @NSManaged public var image: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?

}
