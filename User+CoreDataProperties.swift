//
//  User+CoreDataProperties.swift
//  
//
//  Created by Venkatesh on 7/9/20.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var avatar: String?
    @NSManaged public var designation: String?
    @NSManaged public var name: String?

}
